const express = require('express');
const app = express();
const port = 5000;

app.use(express.json());

const newUser = {
	firstName : 'levi',
	lastName : 'ackerman',
	age : 28,
	contactNo : '09123456789',
	batchNo : 166,
	email : 'leviAckerman@mail.com',
	password : 'thequickbrownfoxjumpsoverthelazydog'
};

// Activity Instructions:
	/*
	1. Assert that the newUser firstName type is a string
	2. Assert that the newUser lastName type is a string
	3. Assert that the newUser firstName is not undefined
	4. Assert that the newUser lastName is not undefined
	5. Assert that the newUser age is at least 18
	6. Assert that the newUser age type is a number
	7. Assert that newUser contact number type is a string
	8. Assert that newUser batch number type is a number
	9. Assert that newUser batch number is not undefined. 
	10. Take a screenshot of your passing tests and send it to our hangouts
	11. Create a group in GitLab called batch-166
	12. Inside the group, create a new project: session1
	13. Push your repository in session1
	14. Link it in Boodle under WDC041-1 | Introduction to Unit Testing and Mocha Chai

*/

module.exports = {
	newUser : newUser
};

app.listen(port, () => {
	console.log(`Server is running at port ${port}`)
});
